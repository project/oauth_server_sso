<?php

/**
 * @file
 * Contains miniOrange Support class.
 */
namespace Drupal\oauth_server_sso;

use Drupal\oauth_server_sso\Utilities;
use Drupal\oauth_server_sso\MiniorangeOAuthServerCustomer;

class MiniorangeOAuthServerSupport {
    public $email;
    public $phone;
    public $query;
    public $query_type;
    public $support_needed_for;
    public $mo_timezone;
    public $mo_date;
    public $mo_time;

    public function __construct($email, $phone, $query, $query_type,$support_needed_for='', $timezone='', $mo_date='', $mo_time='') {
      $this->email = $email;
      $this->phone = $phone;
      $this->query = $query;
      $this->query_type = $query_type;
      $this->support_needed_for = $support_needed_for;
      $this->mo_timezone = $timezone;
      $this->mo_date = $mo_date;
      $this->mo_time = $mo_time;
    }

    /**
     * Send support query.
    */
    public function sendSupportQuery() {
      $modulesInfo = \Drupal::service('extension.list.module')->getExtensionInfo('oauth_server_sso');
      $modulesVersion = $modulesInfo['version'];
      if ($this->query_type == 'Trial Request' || $this->query_type == 'Call Request' || $this->query_type == 'Contact Support') {
        $url = MiniorangeOAuthServerConstants::BASE_URL . '/moas/api/notify/send';
        $request_for = $this->query_type == 'Trial Request' ? 'Trial' : ($this->query_type == 'Contact Support' ? 'Support' : 'Setup Meeting/Call');
        $subject = $request_for.' request for Drupal-' . \DRUPAL::VERSION . ' OAuth Server Module | ' .$modulesVersion;
        $this->query = $request_for.' requested for - ' . $this->query;
        $customerKey = "16555";
        $apikey = "fFd2XcvTGDemZvbw1bcUesNJWEqKbbUq";
        $customer = new MiniorangeOAuthServerCustomer();
        $currentTimeInMillis = $customer->getTimeStamp();
        $stringToHash = $customerKey . $currentTimeInMillis . $apikey;
        $hashValue = hash("sha512", $stringToHash);
        if ($this->query_type == 'Call Request'){
          $content = '<div >Hello, <br><br>Company :<a href="' . $_SERVER['SERVER_NAME'] . '" target="_blank" >' . $_SERVER['SERVER_NAME'] . '</a><br><br>Phone Number:' . $this->phone . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br> Timezone: <b>'. $this->mo_timezone .'</b><br><br> Date: <b>'. $this->mo_date .'</b>&nbsp;&nbsp; Time: <b>'. $this->mo_time .'</b><br><br>Query:[Drupal ' . Utilities::moGetDrupalCoreVersion() . ' OAuth Server Free | PHP '. phpversion() .' | '. $modulesVersion . ' ] ' . $this->query . '</div>';
        } else if ($this->query_type == 'Contact Support') {
            $content = '<div >Hello, <br><br>Company :<a href="' . $_SERVER['SERVER_NAME'] . '" target="_blank" >' . $_SERVER['SERVER_NAME'] . '</a><br><br><strong>Support needed for: </strong>' . $this->support_needed_for . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br>Query:[Drupal ' . Utilities::moGetDrupalCoreVersion() . ' OAuth Server Free | ' . $modulesVersion . ' | PHP ' .phpversion().' ] ' . $this->query . '</div>';
        } else {
          $content = '<div >Hello, <br><br>Company :<a href="' . $_SERVER['SERVER_NAME'] . '" target="_blank" >' . $_SERVER['SERVER_NAME'] . '</a><br><br>Phone Number:' . $this->phone . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br>Query:[Drupal ' . Utilities::moGetDrupalCoreVersion() . ' OAuth Server Free | PHP '. phpversion() .' | ' . $modulesVersion . ' ] ' . $this->query . '</div>';
        }
        $fields = array(
          'customerKey' => $customerKey,
          'sendEmail' => true,
          'email' => array(
            'customerKey' => $customerKey,
            'fromEmail' => $this->email,
            'fromName' => 'miniOrange',
            'toEmail' => MiniorangeOAuthServerConstants::SUPPORT_EMAIL,
            'toName' => MiniorangeOAuthServerConstants::SUPPORT_EMAIL,
            'subject' => $subject,
            'content' => $content
          ),
        );
        $header = array('Content-Type'=> 'application/json',
          'Customer-Key' => $customerKey,
          'Timestamp' => $currentTimeInMillis,
          'Authorization' => $hashValue
        );
        $response = Utilities::callAPI($url, 'POST', $fields, $header);
        return $response;
      }
    }
}