<?php

/**
 * @file
 * Contains \Drupal\oauth_server_sso\Form\MiniorangeAdvancedSettings.
 */
namespace Drupal\oauth_server_sso\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\oauth_server_sso\Utilities;

class MiniorangeAdvancedSettings extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'miniorange_advanced_settings';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        global $base_url;

        $form['markup_library'] = array(
            '#attached' => array(
                'library' => array(
                    "oauth_server_sso/oauth_server_sso.style_settings",
                    "core/drupal.dialog.ajax",
                )
            ),
        );

        $url_path = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('oauth_server_sso'). '/includes/images';
        $form['mo_oauth_server_style'] = array('#markup' => '<div class="mo_oauth_table_layout mo_oauth_container_full">');
        $form['markup_top_oauth_server_idp_initiated'] = array(
            '#type' => 'fieldset',
            '#title' => t('Server/Drupal Initiated Login').'&nbsp;<a href="licensing"><img class="mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium"><span class="mo_pro_tooltip">'.t('Available in the Premium version').'</span></a> <hr>',
        );

        $form['markup_top_oauth_server_idp_initiated']['idp_initiated_ckeckbox'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Enable Server/Drupal Initiated Single Sign On'),
        );

        $form['markup_top_oauth_server_advanced_settings'] = array(
            '#type' => 'fieldset',
            '#title' => t('OpenID Connect Support') . '<hr>',
        );

        $form['markup_top_oauth_server_advanced_settings']['enable_openid_checkbox'] = array(
            '#type' => 'checkbox',
            '#title' => '<b>' . t('Enable OpenID Connect Support') . '</b>',
            '#default_value' => (\Drupal::config('oauth_server_sso.settings')->get('enable_openid'))??true,
            '#description' => t('When this is enabled and the client application uses ').'<i>'.t('\'openid\'').'</i>'.t(' scope, Drupal will send an ').'<i>'.t('\'id_token\'').'</i>'.t(' along with an ').'<i>'.t('\'access_token\'').'</i>'.t(' to the token endpoint response.')

        );

        $form['markup_top_oauth_server_advanced_settings']['token_expiry'] = array(
            '#markup'=>'<b><h7>' . t('ID Token Expiry Time( In seconds ): ') . '</h7></b>' . '<a href="licensing"><img class="mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium"><span class="mo_pro_tooltip">' . t('Available in the Premium version') . '</span></a>'
        );

        $form['markup_top_oauth_server_advanced_settings']['token_expiry']['field'] = array(
            '#type' => 'textfield',
            '#disabled' => true,
            '#default_value' => 3600,
            '#description' => t('By default ID Tokens are valid for 1 hour.'),
        );

        $form['markup_top_oauth_server_advanced_settings']['signing_algos'] = array(
            '#markup'=>'<b><h7>' . t('ID Token Signing Algorithms ') . '</h7></b>' . '<a href="licensing"><img class="mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium"><span class="mo_pro_tooltip">' . t('Available in the Premium version') . '</span></a>'
        );

        $form['markup_top_oauth_server_advanced_settings']['signing_algos']['HS256'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#default_value' => true,
            '#title' => t('HS256 (HMAC using SHA256)'),
        );

        $form['markup_top_oauth_server_advanced_settings']['signing_algos']['RS256'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#default_value' => false,
            '#title' => t('RS256 (RSA using SHA256)'),
        );

        $form['markup_top_oauth_server_advanced_settings']['add_claims'] = array(
            '#markup'=>'<b><h7>' . t('Add optional claims to ID Token ') . '</h7></b>' . '<a href="licensing"><img class="mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium"><span class="mo_pro_tooltip">' . t('Available in the Premium version') . '</span></a>'
        );

        $form['markup_top_oauth_server_advanced_settings']['add_claims']['first']= array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#default_value' => false,
            '#title' => t('As per ').'<i>'.t('\'claims\'').'</i>'.t(' Parameter in the Authorization Endpoint request '),
            '#description' => '<br><br><b>'.t('OR').'</b>',
        );

        $id_token_claims =array(
                    'uid'=>'uid',
                    'uuid'=>'uuid',
                    'name'=>'name',
                    'mail'=>'mail'
        );

        $form['markup_top_oauth_server_advanced_settings']['add_claims']['second'] = array(
            '#type' => 'container',
            '#attributes' => array('class' => array('checkbox-container')),
        );

        foreach ($id_token_claims as $key) {
            $form['markup_top_oauth_server_advanced_settings']['add_claims']['second'][$key] = array(
              '#type' => 'checkbox',
              '#title' => t($key),
              '#default_value' => true,
              '#disabled' => true,
              '#prefix' => '<div class="checkbox-inline">',
               '#suffix' => '</div>',
            );
        }

        $form['markup_top_oauth_server_advanced_settings']['add_claims']['second']['add_more_button'] = array(
            '#type' => 'button',
            '#value' => t('Add more'),
            '#disabled' => true,
            '#attributes' => ['class' => ['button--small button--primary']],
            '#prefix' => '<div class="checkbox-inline">',
             '#suffix' => '</div>'
        );

        $form['mo_oauth_server_save_advanced_settings'] = array(
            "#prefix"=>'<br>',
            '#type' => 'submit',
            '#button_type' => 'primary',
            '#value' => t('Save Settings'),
             "#suffix"=>'</div>'
        );

      Utilities::moOAuthServerShowCustomerSupportIcon($form, $form_state);
      return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        $form_values = $form_state->getValues();
        $enable_openid= $form_values['enable_openid_checkbox'];
        \Drupal::configFactory()->getEditable('oauth_server_sso.settings')->set('enable_openid', $enable_openid)->save();
        \Drupal::messenger()->addMessage($this->t('Configurations saved successfully.'));

    }

}
