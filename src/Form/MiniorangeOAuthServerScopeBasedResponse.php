<?php

namespace Drupal\oauth_server_sso\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\oauth_server_sso\MiniorangeOAuthServerConstants;


class MiniorangeOAuthServerScopeBasedResponse extends FormBase{

    public function getFormId() {
        return 'oauth_server_sso_scope_based_response';
    }
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['#prefix'] = '<div id="modal_scope_based_response_form">';
        $form['#suffix'] = '</div>';

        $form['markup_library'] = array(
            '#attached' => array(
                'library' => array(
                    "oauth_server_sso/oauth_server_sso.style_settings",
                )
            ),
        );

        $form['scope_markup'] = array(
            '#markup' => '<p>'.t('Scope decides range of data being sent from the OAuth server. You can customize response from OAuth Server based on scopes.').'</p>',
        );

        $form['mo_oauth_server_scoped_based_resposne'] = array(
            '#type' => 'table',
            '#tableselect' => true,
            '#js_select' => false,
            '#disabled' => true,
            '#default_value' => ['email' => true, 'openid' => true, 'profile' => true],
            '#header' => ['Scope','Attributes sent in server response'],
            '#attributes' => ['style' => 'border-collapse: separate;'],
            '#empty' => t('Something is not right. Please run the update script or contact us at <a href="mailto:'.MiniorangeOAuthServerConstants::SUPPORT_EMAIL.'">'. MiniorangeOAuthServerConstants::SUPPORT_EMAIL .'</a>'),
            '#responsive' => TRUE ,
        );

        $data = [
            'email' =>	' email',
            'openid' =>	'id_token (including claims - email, username, issuer, subject, audience, issued_at, expiry_time)',
            'profile' => 'username, uid, uuid',
            'user.read' =>	'All configured custom attributes',
            'user.read.roles' => 'All Drupal roles'
        ];

        foreach ($data as $key => $value) {
            $row = self::getScopeBasedResponseTable($key, $value);
            $form['mo_oauth_server_scoped_based_resposne'][$key] = $row;
        }

        $form['markup_top_oauth_server_scope_based_response_note'] = [
            '#markup' => '<b>Note:</b> The <i>user.read</i> and <i>user.read.roles</i> scopes are allowed in <a href="licensing">Premium</a> version of the module<br>'
        ];

        return $form;
    }

    /*
    *Abstract method so need to override it
    */
    public function submitForm(array &$form, FormStateInterface $form_state){}

    public function getScopeBasedResponseTable($scopeKey, $scopeValue){
        $row[$scopeKey] = [
            '#markup' => '<div class="container-inline"><strong>'. $scopeKey . '</strong>',
        ];

        $row[$scopeValue] = [
            '#markup' => '<span>'.$scopeValue.'</span>',
        ];

        return $row;
    }

}
