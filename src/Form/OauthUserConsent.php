<?php

namespace Drupal\oauth_server_sso\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class OauthUserConsent extends ConfirmFormBase {


  /**
   * User ID
   * @var integer
   */
  protected $user_id;

  /**
   * Destination url to redirect the user
   * @var string
   */
  protected string $url;

  /**
   * Scope array that defines the user scope
   * @var array
   */
  private array $scope = [];

  /**
   * The base url of the client
   * @var string|null
   */
  private string|null $redirect_url;

  /**
   * User state
   * @var string|null
   */
  private string|null $state;

  /**
   * {@inheritdoc}
   * Returns form-id
   */
  public function getFormId() : string {
    return "oauth_user_consent_confirm";
  }

  /**
   * Build form
   *
   * @param array $form form array
   * @param FormStateInterface $form_state form state object
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {

    $request_query = \Drupal::request()->query;
    $this->user_id = $request_query->get('user');
    $this->url = $request_query->get('url');
    $this->scope = explode(' ', $request_query->get('scope') ?? []);
    $this->redirect_url = $request_query->get('redirect_url');
    $this->state = $request_query->get('state');

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit form
   *
   * @param array $form form array
   * @param FormStateInterface $form_state form state object
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if(!empty($this->url)){
      $response = new TrustedRedirectResponse($this->url);
      $response->setMaxAge(0);
      $form_state->setResponse($response);
    }
  }

  /**
   * Return cancel url
   *
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url
  {
    return Url::fromUri($this->redirect_url, ["query" => [ "error" => "access_denied", "error_description" => "The user denied the consent request.", "state" => $this->state]]);
  }

  /**
   * get question of consent screen
   *
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup
  {
    return $this->t('User OAuth Consent');
  }

  /**
   * Get description of the consent screen
   *
   * @return TranslatableMarkup
   */
  public function getDescription(): TranslatableMarkup
  {
    $site_name = \Drupal::config('system.site')->get('name');
    $content = 'By allowing <b>'.$site_name.'</b> will share your name, email address and other related information.<br><br>';

    return $this->t($content);
  }

  /**
   * returns confirm text
   *
   * @return TranslatableMarkup
   */
  public function getConfirmText(): TranslatableMarkup
  {
    return $this->t('Allow');
  }

  /**
   * Return cancel text
   *
   * @return TranslatableMarkup
   */
  public function getCancelText(): TranslatableMarkup
  {
    return $this->t('Deny');
  }

}
