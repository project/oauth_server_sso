<?php
/**
 * @file
 * Contains Licensing information for miniOrange OAuth Server Login Module.
 */

 /**
 * Showing Licensing form info.
 */
namespace Drupal\oauth_server_sso\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\oauth_server_sso\Utilities;

class MiniorangeLicensing extends FormBase {

    public function getFormId() {
        return 'oauth_server_sso_licensing';
    }

    public function buildForm(array $form, FormStateInterface $form_state){
        global $base_url;
        $form['markup_library'] = array(
            '#attached' => array(
                'library' => array(
                    "oauth_server_sso/oauth_server_sso.style_settings",
                    "core/drupal.dialog.ajax",
                )
            ),
        );

        $customers = 'https://plugins.miniorange.com/drupal#customer';
        $module_path=\Drupal::service('extension.list.module')->getPath("oauth_server_sso");
        $form['mo_oauth_server_style'] = array('#markup' => '<div><div class="mo_oauth_table_layout">');
        $form['miniorage_oauth_server_customers'] = array(
            '#prefix'=> '<br>',
            '#type' => 'link',
            '#title' => t('Organizations that Trust miniOrange'),
            '#url' => Url::fromUri($customers),
            '#attributes' => ['class' => ['button', 'button--primary mo_organization_trust_button'], 'target' => '_blank',],
            '#suffix' => '</h3>'
        );

        $features = [
            [ Markup::create('<h2>'.t('FEATURES / PLANS').'</h2>'), Markup::create(t('<br><h2>'.t('FREE').'</h2> <p class="mo_oauth_pricing-rate"><sup>$</sup> 0</p>')), Markup::create('<br><h2>'.t('PREMIUM').'</h2><p class="mo_oauth_pricing-rate"> <h5>'.t('User Slab:').'</h5> <select class="form-select"><option value="1 - 100">1 - 100 : $450 / year</option><option value="101 - 200">101 - 200 : $550 / year</option><option value="201 - 300">201 - 300 : $650 / year</option><option value="301 - 400">301 - 400 : $750 / year</option><option value="401 - 500">401 - 500 : $850 / year</option><option value="501 - 1000">501 - 1000 : $1250 / year</option><option value="1001 - 2000">1001 - 2000 : $1600 / year</option><option value="2001 - 3000">2001 - 3000 : $1900 / year</option><option value="3001 - 4000">3001 - 4000 : $2150 / year</option><option value="4001 - 5000">4001 - 5000 : $2400 / year</option><option value="5000+">5000+ Users - Contact Us</option></select></p>'),],
            [ '', Markup::create(t('<a class="button" disabled>'.t('You are on this Plan').'</a>')), Markup::create(t('<a class="button button--primary" href="mailto:drupalsupport@xecurify.com" target="_blank">'.t('Contact Us').'</a>'))],
            [ Markup::create(t('Basic Attribute Mapping')), Markup::create(t('&#x2714;')), Markup::create(t('&#x2714;')),],
            [ Markup::create(t('Authorization Code Grant')), Markup::create(t('&#x2714;')), Markup::create(t('&#x2714;')),],
            [ Markup::create(t('OpenID Connect Support')), Markup::create(t('&#x2714;')), Markup::create(t('&#x2714;')),],
            [ Markup::create(t('Enable/Disable Master Switch')), Markup::create(t('&#x2714;')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Server/Drupal Initiated Login')), Markup::create(t('-')), Markup::create(t('&#x2714;')),],
            [ Markup::create(t('Customized Claims in ID Token')), Markup::create(t('-')), Markup::create(t('&#x2714;')),],
            [ Markup::create(t('Disable Redirect URI validation')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Custom Attribute Mapping')), Markup::create(t('-')), Markup::create(t('&#x2714;')),],
            [ Markup::create(t('Support for Headless integration')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Resource Owner Credentials Grant (Password Grant)')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Client Credentials Grant')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Implicit Grant')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Refresh token Grant')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Customized Token Length')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Enforce State parameter')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Extended OAuth API support')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Support for Asymmetric JWT Signing Algorithms')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Error Logging')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Multi-site Support')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Login Reports / Analytics')), Markup::create(t('-')), Markup::create(t('&#x2714;'))],
            [ Markup::create(t('Support for multiple OAuth / OIDC Clients')), Markup::create(t('-')), Markup::create(t('&#x2714; *'))],

        ];

        $form['miniorage_oauth_server_feature_list'] = array(
            '#type' => 'table',
            '#responsive' => TRUE,
            '#rows' => $features,
            '#size' => 3,
            '#attributes' => ['class' => ['mo_upgrade_plans_features']],
            '#prefix' => '<hr><br>',
        );

        $form['miniorage_oauth_server_instance_based'] = array(
            '#markup' => '<br><div class="mo_instance_note"> <b>*</b>'.t(' There is an additional cost for the OAuth Clients if the number of OAuth Client is more than 1.').'</div>',
        );

        $rows = [
            [ Markup::create(t('<b>1.</b>'.t(' Click on Upgrade Now button for required licensed plan and you will be redirected to miniOrange login console.').'</li>')), Markup::create('<b>5.</b>'.t(' Uninstall and then delete the free version of the module from your Drupal site.')) ],
            [ Markup::create(t('<b>2.</b>'.t(' Enter your username and password with which you have created an account with us. After that you will be redirected to payment page.'))), Markup::create(t('<b>6.</b>'.t(' Now install the downloaded licensed version of the module.'))) ],
            [ Markup::create(t('<b>3.</b>'.t(' Enter your card details and proceed for payment. On successful payment completion, the Licensed version module(s) will be available to download.'))), Markup::create(t('<b>7.</b>'.t(' Clear Drupal Cache from').' <a href="'.$base_url.'/admin/config/development/performance" >'.t('here').'</a>.')) ],
            [ Markup::create(t('<b>4.</b>'.t(' Download the licensed module(s) from Module Releases and Downloads section.'))), Markup::create(t('<b>8.</b>'.t(' After enabling the licensed version of the module, login using the account you have registered with us.'))) ],
        ];

        $form['miniorage_oauth_server_how_to_upgrade'] =[
            '#markup' => t('<br><hr><br>'),
        ];

        $form['miniorage_oauth_server_how_to_upgrade_table'] = array(
            '#type' => 'table',
            '#responsive' => TRUE,
            '#header' => [
                'how_to_upgrade' => [
                    'data' => t('HOW TO UPGRADE TO LICENSED VERSION MODULE'),
                    'colspan' => 2,
                ],
            ],
            '#rows' => $rows,
            '#attributes' => ['style' => 'border:groove', 'class' => ['mo_how_to_upgrade']],
        );

        $form['website_security_payment_methods'] = array(
            '#markup' => '<div class="container mo-container payment_method_main_divs" id="payment_method">
            <h3 style="text-align: center; margin:3%;">'.t('PAYMENT METHODS').'</h3><hr><br><br>'
        );
        $form['card_method'] = array(
            '#markup' => '<div class="row"><div class="col-md-3 payment_method_inner_divs">
            <div><img src="'. $base_url . '/' . $module_path . '/includes/images/card_payment.png" width="120" ></div><hr>
            <p>'.t('If the payment is made through Credit Card/International Debit Card, the license will be created automatically once the payment is completed.').'</p>
            </div>'
        );
        $form['bank_transfer_method'] = array(
            '#markup' => '<div class="col-md-3 payment_method_inner_divs">
            <div><img src="'. $base_url . '/' . $module_path . '/includes/images/bank_transfer.png" width="150" ></div><hr>
            <p>'.t('If you want to use bank transfer for the payment then contact us at ').'<a target="_blank" href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a>'.t(' so that we can provide you the bank details.').'</p>
            </div>'
        );

        $form['return_policy'] = array(
            '#markup' => '</div><div class="mo_return_policy" style="text-align: center; margin:3%;"><h3>'.t('Return Policy').'</h3><hr></div>
            <p>'.t('At miniOrange, we want to ensure you are 100% happy with your purchase. If the premium module you purchased is not working as advertised and you have attempted to resolve any issues with our support team, which could not get resolved. We will refund the whole amount within 10 days of the purchase. Please email us at ').'<a target="_blank" href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a>'.t(' for any queries regarding the return policy.').'</p><b>'.t('Note that this policy does not cover the following cases:').'</b><br><br>
            <li>'.t('Change of mind or change in requirements after purchase.').'</li>
            <li>'.t('Infrastructure issues not allowing the functionality to work.').'</li></p>'
        );

        $form['payment_end'] = array(
            '#markup' => '</div>'
        );

        $form['main_layout_div_end_1'] = array(
            '#markup' => '</div></div>',
        );
       Utilities::moOAuthServerShowCustomerSupportIcon($form, $form_state);

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {

    }
}

