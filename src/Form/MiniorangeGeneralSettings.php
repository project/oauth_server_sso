<?php

/**
 * @file
 * Contains \Drupal\oauth_server_sso\Form\MiniorangeGeneralSettings.
 */
namespace Drupal\oauth_server_sso\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\oauth_server_sso\Utilities;
use Drupal\oauth_server_sso\MiniorangeOAuthServerConstants;
use Drupal\Core\Url;
class MiniorangeGeneralSettings extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'miniorange_general_settings';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        global $base_url;

        $form['markup_library'] = array(
            '#attached' => array(
                'library' => array(
                    "oauth_server_sso/oauth_server_sso.style_settings",
                    "core/drupal.dialog.ajax",
                )
            ),
        );

        $url_path = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('oauth_server_sso'). '/includes/images';

        $form['mo_oauth_server_style'] = array('#markup' => '<div class="mo_oauth_table_layout mo_oauth_container_full">');
        $form['markup_top_oauth_server_master_key'] = array(
            '#type' => 'fieldset',
            '#title' => t('Master Switch').'<hr>',
        );

        $form['markup_top_oauth_server_master_key']['master_key_checkbox']= array(
            '#type' => 'checkbox',
            '#default_value' => (\Drupal::config('oauth_server_sso.settings')->get('enable_oauth_server'))??true,
            '#title' => t('Enable OAuth Server'),
            '#description' => t('Disabling this option will stop sending/receiving API calls from/to your OAuth Client application.'),
        );

        $form['markup_top_oauth_server_master_key']['save_chanes'] = array(
            '#type' => 'submit',
            '#button_type' => 'primary',
            '#value' => t('Save Settings'),
        );

        $form['markup_top_oauth_server_general_settings'] = array(
            '#type' => 'fieldset',
            '#title' => t('General Settings  ').'&nbsp;<a href="licensing"><img class="mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium"><span class="mo_pro_tooltip">'.t('Available in the Premium version').'</span></a><hr>',
        );

        $form['markup_top_oauth_server_general_settings']['oauth_server_sso_accesstoken_expiry'] = array(
            '#type' => 'textfield',
            '#title' => t('Access Token Expiry Time ( In seconds ):'),
            '#disabled' => true,
            '#default_value' => 3600,
            '#description' => t('By default Access Tokens are valid for 1 hour.'),
        );

        $form['markup_top_oauth_server_general_settings']['oauth_server_sso_refreshtoken_expiry'] = array(
            '#type' => 'textfield',
            '#title' => t('Refresh Token Expiry Time ( In seconds ):'),
            '#disabled' => true,
            '#default_value' => 86400,
            '#description' => t('By default Refresh Tokens are valid for 24 hours.'),
        );

        $form['markup_top_oauth_server_general_settings']['token_length'] = array(
            '#type' => 'textfield',
            '#title' => t('Token Length:'),
            '#disabled' => true,
            '#default_value' => 127,
        );

        $form['markup_top_oauth_server_redirect_uri'] = array(
            '#type' => 'fieldset',
            '#title' => t('Redirect/Callback URI Validation ').'<a href="licensing"><img class="mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium"><span class="mo_pro_tooltip">'.t('Available in the Premium version').'</span></a><hr>',
        );

        $form['markup_top_oauth_server_redirect_uri']['oauth_server_sso_enable_callback_validation'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Require Exact Redirect URI'),
            '#default_value' => true,
            '#description' => '<b>'.t('Note: ').'</b>'.t('Disable this option to skip the validation of Callback/Redirect URL.'),
        );

        $form['markup_top_oauth_server_additional_parameters'] = array(
            '#type' => 'fieldset',
            '#title' => t('Authorization Endpoint Parameters ').'&nbsp; <a href="licensing"><img class="mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium"><span class="mo_pro_tooltip">'.t('Available in the Premium version').'</span></a><hr>',
        );

        $form['markup_top_oauth_server_additional_parameters']['oauth_server_response_type'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#default_value' => true,
            '#title' => t('Response type '),
            '#description' => t('This version of the module only supports the ').'<i>\'code\'</i>'.t('as a value in the ').'<i>'.t('\'response_type\'').'</i>'.t('parameter. The other values i.e. ').'<i>'.t('\'token\'').'</i>'.t('and').'<i>'.t('\'id_token\'').'</i>'.t('are supported in the premium version of the module.'),
        );

        $form['markup_top_oauth_server_additional_parameters']['oauth_server_sso_enforce_state_parameter'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Enforce State Parameter '),
            '#description' => t('When enabled, the authorization request will fail if ').'<i>'.t('\'state\'').'</i>'.t(' parameter is not providedor or is invalid.'),
        );

        $form['markup_top_oauth_server_additional_parameters']['oauth_server_sso_nonce'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Nonce '),
            '#description' => t('It is a random value that the client application can add to the authorization request in the ').'<i>'.t('\'nonce\'').'</i>'.t('parameter. The same value is included in the ID token claims that Server/Drupal issues when this option is enabled.'),
        );
        $form['markup_top_oauth_server_additional_parameters']['oauth_server_display'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Display'),
            '#description' => t('If Checked, the Server validates the ').'<i>'.t('\'display\'').'</i>'.t(' parameter from the authorization request and displays the authentication and consent user interface pages to the End-User accordingly. The supported values are ').'<i>'.t('\'page\'').'</i>'.t(', ').'<i>'.t('\'popup\'').'</i>'.t(', ').'<i>'.t('\'touch\'').'</i>'.t(', and ').'<i>'.t('\'wap\'').'</i>'.t('. If the ').'<i>'.t('\'display\'').'</i>'.t(' parameter is not specified, ').'<i>'.t('\'page\'').'</i>'.t(' is the default display mode.')
        );
        $form['markup_top_oauth_server_additional_parameters']['oauth_server_prompt'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Prompt '),
            '#description' => t('The ').'<i>'.t('\'prompt\'').'</i>'.t(' parameter is used to specify whether the server should prompt the user for reauthentication or consent. The possible values include ').'<i>'.t('\'none\'').'</i>'.t(', ').'<i>'.t('\'login\'').'</i>'.t(', ').'<i>'.t('\'consent\'').'</i>'.t(', and ').'<i>'.t('\'select_account\'').'</i>.'
        );

        $form['markup_top_oauth_server_additional_parameters']['oauth_server_login_hint'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Login hint '),
            '#description' => t('The purpose of the ').'<i>'.t('\'login_hint\'').'</i>'.t(' parameter is to streamline the user\'s authentication experience by prefilling username or email fields on the authentication screen.')
        );

        $form['markup_top_oauth_server_additional_parameters']['oauth_server_response_mode'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Response mode '),
            '#description' => t('The ').'<i>'.t('\'response_mode\'').'</i>'.t(' parameter is used to specify how the server should return the authorization response. The possible values include ').'<i>'.t('\'query\'').'</i>'.t(', ').'<i>'.t('\'fragment\'').'</i>'.t(', and ').'<i>'.t('\'form_post\'').'</i>.'
        );

        $form['markup_top_oauth_server_additional_parameters']['oauth_server_ui_locales'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('UI locales '),
            '#description' => t('The ').'<i>'.t('\'ui_locales\'').'</i>'.t(' parameter is used to specify the preferred language for the user interface on the authentication screen.')
        );

        $form['markup_top_oauth_server_troubleshoot'] = array(
            '#type' => 'fieldset',
            '#title' => t('Debugging & Troubleshoot ').'&nbsp;<a href="licensing"><img class="mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium"><span class="mo_pro_tooltip">'.t('Available in the Premium version').'</span></a><hr>',
        );

        $form['markup_top_oauth_server_troubleshoot']['oauth_server_sso_enable_loggers'] = array(
            '#type' => 'checkbox',
            '#disabled' => true,
            '#title' => t('Enable logging'),
            '#description' => t('Enabling this checkbox will add loggers under the '). '<a href="' . Url::fromRoute('dblog.overview')->toString() . '" target="_blank">' . t('Reports') . '</a>' . t(' section'),
        );

        $form['markup_top_oauth_server_troubleshoot']['enable_logging_download'] = array(
            '#type' => 'submit',
            '#value' => t('Download Logs'),
            '#button_type' => 'primary',
            '#disabled' => true,

          );
        $form['mo_oauth_server_save_general_settings'] = array(
            '#type' => 'submit',
            '#disabled' => true,
            '#button_type' => 'primary',
            '#value' => t('Save Settings'),
            '#attributes' => array('style' => '	margin: auto; display:block; '),
        );

        $form['mo_oauth_server_save_general_settings_close'] = array(
              '#markup'=>'</div>'
        );

        Utilities::moOAuthServerShowCustomerSupportIcon($form, $form_state);

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        $form_values = $form_state->getValues();
        $enable_oauth_server= $form_values['master_key_checkbox'];
        \Drupal::configFactory()->getEditable('oauth_server_sso.settings')->set('enable_oauth_server', $enable_oauth_server)->save();
        \Drupal::messenger()->addMessage($this->t('Configurations saved successfully.'));

    }
}
