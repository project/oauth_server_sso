<?php

/**
 * @file
 * This class represents constants used throughout project.
 */

namespace Drupal\oauth_server_sso;

class MiniorangeOAuthServerConstants {

  const BASE_URL = 'https://login.xecurify.com';
  const SUPPORT_EMAIL = 'drupalsupport@xecurify.com';
}
