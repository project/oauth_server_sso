function CopyToClipboard(element){
    jQuery(".selected-text").removeClass("selected-text");
    let textToCopy = element.innerText;
    navigator.clipboard.writeText(textToCopy);
    jQuery(element).addClass("selected-text");
}

jQuery(window).click(function(e) {
    if( e.target.className === undefined || e.target.className.indexOf("copy_button") === -1)
        jQuery(".selected-text").removeClass("selected-text");
});


jQuery(document).ready(function() {
    jQuery('#grant_type_checkboxes input[value="authorization_code"]').prop('checked', true);
    jQuery('#copy_button_id').click(function () {
        var client_id = document.getElementById('copy_client_id').innerText.trim();
        navigator.clipboard.writeText(client_id);
        var tooltip = document.getElementById("copyTooltip");
        tooltip.innerHTML = "Copied";
       setTimeout(function() {
        tooltip.innerHTML = "Copy to Clipboard";
      }, 1000);
      });

      jQuery('#copy_button_secret').click(function () {
        var copyClientSecret = document.getElementById("copy_client_secret");
        var attrValue = copyClientSecret.getAttribute("attr");
        navigator.clipboard.writeText(attrValue);
        var tooltip = document.getElementById("secretTooltip");
        tooltip.innerHTML = "Copied";
       setTimeout(function() {
        tooltip.innerHTML = "Copy to Clipboard";
      }, 1000);
      });

      jQuery('#show_secret_button').click(function () {
        var tooltipElement = document.getElementById("showsecretTooltip");
        var copyClientSecret = document.getElementById("copy_client_secret");
        var attrValue = copyClientSecret.getAttribute("attr");
        var clientsecretElement = document.getElementById("copy_client_secret");
        var secretButton = document.getElementById('secret_button_text');
        var secretButton_name = secretButton.innerHTML;
        if(secretButton_name == 'View'){
            console.log('view');
            secretButton.innerHTML = 'Hide';
            tooltipElement.innerHTML = "Hide Client Secret";
            clientsecretElement.innerHTML = attrValue;
        }else{
            console.log('hide');
            secretButton.innerHTML = 'View';
            tooltipElement.innerHTML = "Show Client Secret";
            var hidesecret = copyClientSecret.getAttribute("attrHide");
            clientsecretElement.innerHTML = hidesecret;
        }

      });
});